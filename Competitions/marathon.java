import java.io.*;
import java.util.*;



class OlympiadIO {
    private int currentPos = 0;
    private ArrayList<String> inLines = new ArrayList<String>();
    private final String name;
    
    private ArrayList<String> outLines = new ArrayList<String>();
    
    
    
    public OlympiadIO(String name) {
        this.name = name;
        inputSetUp();
        
        
        
    }
    
    protected void inputSetUp() {
        try{
            BufferedReader in;
            in = new BufferedReader(new FileReader(name + ".in"));
            
            String temp;
            do {
                temp = in.readLine();
                if(temp != null) {
                    inLines.add(temp);
                }
                
            } while(temp != null);
        } catch (IOException e) {
            
        }
    }
    protected ArrayList<String> getInLines() {return inLines; }
    protected ArrayList<String> getOutLines() {return outLines;}
    
    public String read(int lineNum) {
        currentPos = lineNum;
        return (String) inLines.get(lineNum);
    }
    
    public String readNext() {
        
        
        currentPos++;
        return read(currentPos);
        
        
    }
    public String reread() {
        return read(currentPos);
    }
    
    public int getNumOfLines() {
        return inLines.size();
    }
    
    public void addLine(String line) {
        outLines.add(line);
    }
    
    public String[] getTokens(int lineNumber) {
        StringTokenizer st = new StringTokenizer(this.reread());
        ArrayList<String> tokens = new ArrayList<String>();
        String temp;
        while(true) {
            try {
                temp = st.nextToken();
            } catch(NoSuchElementException e) {
                break;
            }
            
            
            tokens.add(temp);
            
            
        }
        
        return tokens.toArray(new String[tokens.size()]);
        
    }
    
    public String[] getTokens() {
        return getTokens(currentPos);
    }
    
    
    public void submit() {
        try{
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
            for(String s: outLines) {
                out.println(s);
            }
            
            out.close();
            System.exit(0);
        }catch (IOException e ) {
            
        }
        
    }
    
    
}
public class marathon{
    public static void main(String[] args) {
        OlympiadIO io = new OlympiadIO("marathon") {//comment out when submitting
            
            @Override
            public void submit() {
                for(String s : getOutLines()) {
                    System.out.println(s);
                }
            }
            
            @Override
            protected void inputSetUp() {
                this.getInLines().addAll(
                                         Arrays.asList(
                                                       new String[] {
                                                           "4",
                                                           "0 0",
                                                           "8 3",
                                                           "11 -1",
                                                           "10 0"
                                                           
                                                       })
                                         );
                
                
                
                
                
            }
        };
        
        
        int numOfLoc = Integer.parseInt(io.read(0));
        Location[] locs = new Location[numOfLoc];
        
        for(int i = 0; i < numOfLoc; i++) {
            io.readNext();
            int x = Integer.parseInt(io.getTokens(i)[0]);
            int y = Integer.parseInt(io.getTokens(i )[1]);
            
            
            locs[i] = new Location(x,y);
        }
        
        Trip t = new Trip(locs);
        t.skipCheckPoint();
        io.addLine(Integer.toString(t.getDistance()));
        
        
        
        
        io.submit();
        
        
        
    }
    
    
}
class Trip {
    private Location[] locations;
    
    
    public Trip(Location[] locations) {
        this.locations = locations;
        
        for(int i = 0; i < locations.length; i++) {
            Location l = locations[i];
            if(i == 0) {//the beginning
                l.setPrevious(l);//sets the previous to itself
                l.setNext(locations[i + 1]);
            } else if (i == locations.length - 1){//the last one
                l.setPrevious(locations[i - 1]);
                l.setNext(l);//sets the end to itself
                
            } else {
                l.setPrevious(locations[ i - 1]);
                l.setNext(locations[i + 1]);
            }
        }
        
    }
    
    public void skipCheckPoint() { //does the skip, and updates the locations
        int[] distance = new int[locations.length];
        
        for(int i = 0; i < locations.length; i++) {//fills in distance
            distance[i] = locations[i].distanceGainedIfOmitted();
        }
        
        int max = 0;
        int index = 0;
        for(int i = 0; i < locations.length; i++) {//finds the best one to skip
            if(distance[i] > max) {
                max = distance[i];
                index = i;
                
            }
        }
        Location skip = locations[index];//the one we will skip
        
        //remove their references from the neighbors
        skip.getPrevious().setNext(
                                   skip.getNext()
                                   );
        skip.getNext().setPrevious(
                                   skip.getPrevious()
                                   );
        
        Location[] withSkip = new Location[locations.length - 1]; //copying into new arry
        for(int i = 0; i < withSkip.length; i++) {
            if(i < index) {
                withSkip[i] = locations[i];
            } else  {
                withSkip[i] = locations[i + 1];
            } //doesn't copy if it lands on the one we skipped
        }
        
        locations = withSkip;// we have now killed the skipped location
    }
    
    public int getDistance() { //calculates the distance she has to travel
        int total = 0;
        for(Location l: locations) {
            total += l.getDistToNext();
        }
        return total;
    }
    
    
    
}

class Location {
    private final int X;
    private final int Y;
    
    private Location previous;
    private Location next;
    
    public Location(int x, int y) {
        X = x;
        Y = y;
    }
    
    public void setPrevious(Location l) {
        this.previous = l;
    }
    
    public void setNext(Location l) {
        this.next = l;
    }
    
    
    public int getX() {return X;}
    public int getY() {return Y;}
    
    public Location getNext() {return this.next;}
    public Location getPrevious() {return this.previous;}
    
    public int manHatDis(Location other) {
        return Math.abs(other.getX() - this.X) + Math.abs(other.getY() - this.Y);
    }
    
    public int getDistToNext() {
        return this.manHatDis(next);
    }
    
    public int distanceGainedIfOmitted() { //should always be positive
        int withMe = this.manHatDis(previous) + this.manHatDis(next);
        int withoutMe = previous.manHatDis(next);
        
        return withMe - withoutMe;
        
    }
}