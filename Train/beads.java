/*NAME: johnwan3
 LANG: JAVA
 TASK: beads
 */



import java.io.*;
import java.util.*;



class OlympiadIO {
    private int currentPos = 0;
    private ArrayList<String> inLines = new ArrayList<String>();
    private final String name;
    
    private ArrayList<String> outLines = new ArrayList<String>();
    
    
    
    public OlympiadIO(String name) {
        this.name = name;
        inputSetUp();
        
        
        
    }
    
    protected void inputSetUp() {
        try{
        BufferedReader in;
        in = new BufferedReader(new FileReader(name + ".in"));
        
        String temp;
        do {
            temp = in.readLine();
            if(temp != null) {
                inLines.add(temp);
            }
            
        } while(temp != null);
        } catch (IOException e) {
            
        }
    }
    protected ArrayList<String> getInLines() {return inLines; }
    protected ArrayList<String> getOutLines() {return outLines;}
    
    public String read(int lineNum) {
        currentPos = lineNum;
        return (String) inLines.get(lineNum);
    }
    
    public String readNext() {
        
        
        currentPos++;
        return read(currentPos);
        
        
    }
    public String reread() {
        return read(currentPos);
    }
    
    public int getNumOfLines() {
        return inLines.size();
    }
    
    public void addLine(String line) {
        outLines.add(line);
    }
    
    public String[] getTokens(int lineNumber) {
        StringTokenizer st = new StringTokenizer(this.reread());
        ArrayList<String> tokens = new ArrayList<String>();
        String temp;
        while(true) {
            try {
                temp = st.nextToken();
            } catch(NoSuchElementException e) {
                break;
            }
            
            
            tokens.add(temp);
            
            
        }
        
        return tokens.toArray(new String[tokens.size()]);
        
    }
    
    public String[] getTokens() {
        return getTokens(currentPos);
    }
    
    
    public void submit() {
        try{
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        for(String s: outLines) {
            out.println(s);
        }
        
        out.close();
        System.exit(0);
        }catch (IOException e ) {
            
        }
        
    }
    
    
}
public class beads{
    public static void main(String[] args) {
        OlympiadIO io = new OlympiadIO("beads")/* {
                                                          
            @Override
            public void submit() {
                for(String s : getOutLines()) {
                System.out.println(s);
                }
            }

            @Override
            protected void inputSetUp() {
                this.getInLines().addAll(
                    Arrays.asList(
                        new String[] {
                        "29",
                            "wwwbbrwrbrbrrbrbrwrwwrbwrwrrb"
                    })
                );





            }
        }*/;
        int max = 0;
        BrokenNecklace[] bn = new BrokenNecklace[Integer.parseInt(io.read(0))];//array of all possible broken necklaces
        
        Necklace necklace = new Necklace(io.readNext());
        
        
        
        for(int i = 0; i < bn.length; i++) {
            bn[i] = necklace.breakAt(i, i + 1);//remember to have breakAt() handle when i + 1 is out of bounds
        }
        
        for(BrokenNecklace b : bn) {
            if(b.getMaxBeads() > max) {
                max = b.getMaxBeads();
            }
        }
        
        io.addLine(String.valueOf(max));
        
        io.submit();
        
        
        
    }
    
  
}

class Necklace {
    private char[] beads;
    
    public Necklace(String beads) {
        this.beads = beads.toCharArray();
    }
    
    public BrokenNecklace breakAt(int lBead, int rBead) {
        List<Beads> beadList = new ArrayList<Beads>();
        
        for(char c: this.beads) {
            beadList.add(new Beads(c));
        }
        
        if(rBead != this.beads.length) { //if it's at the end, it doesn't need to change
            List<Beads> temp = beadList.subList(rBead, beads.length);
            temp.addAll(beadList.subList(0,rBead));//bascially switces
            beadList = temp;
        }
        
        
        
        return new BrokenNecklace(beadList);
        
        
    }
}
class BrokenNecklace{
    List<Beads> beads = new ArrayList<Beads>();
    
    public BrokenNecklace(List<Beads> beads ) {
        for(Beads b: beads ) {
            this.beads.add((Beads)b.clone());
        }
        
        
        Beads firstBead = beads.get(0);
        for(int i = 0; i < beads.size(); i++) {//to make sure the first one isn't white
            if(beads.get(i).getColor() != 'w') {
                firstBead.paint(beads.get(i).getColor());//finds the first non-white and paints it that color
                break;
            }
        }
        
        for(int i = 1; i < beads.size(); i++) {//now that the first one isn't white, it this should work fine
            beads.get(i).paint(beads.get(i - 1).getColor());
            
        }
    }
    
    
    public int getMaxBeads() {
        
        
        boolean isGoneOnceAlready = false;
        
        for(int i = 1; i < beads.size(); i++) {
            if(!beads.get(i).equals(beads.get(i - 1))) {
                if(isGoneOnceAlready) {
                    return i;
                } else {
                    isGoneOnceAlready = true;
                }
                
            }
            
        }
        return beads.size();
    }

}

class Beads implements Cloneable{
    private char color;
    
    public Beads(char color) {
        this.color = color;
    }
    
    public char getColor() {
        return color;
    }
    
    @Override
    public boolean equals(Object o ) {
        return this.color ==((Beads )o).getColor();
    }
    
    @Override
    public Object clone() /*throws CloneNotSupportedException*/ {
        return new Beads(this.color);
    }
    public void paint(char color) {
        if(this.color == 'w') {
            this.color = color;
            
        }
    }
}
