/*NAME: johnwan3
 LANG: JAVA
 TASK: /*name*/
 */

import java.io.*;
import java.util.*;



class OlympiadIO {
    private int currentPos = 0;
    private ArrayList<String> inLines = new ArrayList<String>();
    private final String name;
    
    private ArrayList<String> outLines = new ArrayList<String>();
    
    
    
    public OlympiadIO(String name) {
        this.name = name;
        inputSetUp();
        
        
        
    }
    
    protected void inputSetUp() {
        try{
        BufferedReader in;
        in = new BufferedReader(new FileReader(name + ".in"));
        
        String temp;
        do {
            temp = in.readLine();
            if(temp != null) {
                inLines.add(temp);
            }
            
        } while(temp != null);
        } catch (IOException e) {
            
        }
    }
    protected ArrayList<String> getInLines() {return inLines; }
    protected ArrayList<String> getOutLines() {return outLines;}
    
    public String read(int lineNum) {
        currentPos = lineNum;
        return (String) inLines.get(lineNum);
    }
    
    public String readNext() {
        
        
        currentPos++;
        return read(currentPos);
        
        
    }
    public String reread() {
        return read(currentPos);
    }
    
    public int getNumOfLines() {
        return inLines.size();
    }
    
    public void addLine(String line) {
        outLines.add(line);
    }
    
    public String[] getTokens(int lineNumber) {
        StringTokenizer st = new StringTokenizer(this.reread());
        ArrayList<String> tokens = new ArrayList<String>();
        String temp;
        while(true) {
            try {
                temp = st.nextToken();
            } catch(NoSuchElementException e) {
                break;
            }
            
            
            tokens.add(temp);
            
            
        }
        
        return tokens.toArray(new String[tokens.size()]);
        
    }
    
    public String[] getTokens() {
        return getTokens(currentPos);
    }
    
    
    public void submit() {
        try{
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        for(String s: outLines) {
            out.println(s);
        }
        
        out.close();
        System.exit(0);
        }catch (IOException e ) {
            
        }
        
    }
    
    
}
public class /* name */{
    public static void main(String[] args) {
        OlympiadIO io = new OlympiadIO(/* name */) {//comment out when submitting
                                                          
            @Override
            public void submit() {
                for(String s : getOutLines()) {
                System.out.println(s);
                }
            }

            @Override
            protected void inputSetUp() {
                this.getInLines().addAll(
                    Arrays.asList(
                        new String[] {
                        "20"
                    })
                );





            }
        };

        
        
        
        io.submit();
        
        
        
    }
    
  
}
