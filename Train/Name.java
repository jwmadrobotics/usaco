package main;

public class Name {
	public static final int MAGIC_NUM = 47;
	
	private int value;
	private String name;
	
	
	
	/*never se this*/private static final String ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String[] ABC_ARRAY;

	static /*laziness*/{
		ABC_ARRAY = ABC.split("");//should make an array of strings with each string containing 1 letter
		//value should be index + 1
	}
	
	
	
	public Name(String name) {
		this.name = name;
		
		String[] nameArray = name.split("");
		int[] nameValues = new int[nameArray.length];
		
		//this loop fills up nameValues with its correct values
		for(int i = 0; i < nameValues.length; i++) { //cycles through the name
			for(int j = 0; j < ABC_ARRAY.length; j++ ) {//cycles through the alphabet, finding the match
			
				if(nameArray[i].equals(ABC_ARRAY[j])) {
					nameValues[i] = j + 1; //+1 because arrays start at 0
                    break;
				}
			}
		}
		
		
		value = 1; //because multiplying by 0 is 0
		for(int i: nameValues) {
			value *= i;
		}
		
		
	}
	
	public int getValue() {
		return value;
	}

	public boolean isAbleToGoWith(Name other) {
		return this.value % MAGIC_NUM == other.getValue() % MAGIC_NUM;
			
		
	}
	
	

}
