/*NAME: johnwan3
 LANG: JAVA
 TASK: gift1
*/

import java.io.*;
import java.util.*;

class OlympiadIO {
    private int currentPos = 0;
    private final ArrayList<String> inLines = new ArrayList<String>();
    
    private final PrintWriter OUT;
    private final ArrayList<String> outLines = new ArrayList<String>();
    
    
    public OlympiadIO(String name) throws IOException{
        BufferedReader in;
        in = new BufferedReader(new FileReader(name + ".in"));
        
        String temp;
        do {
            temp = in.readLine();
            if(temp != null) {
                inLines.add(temp);
            }
            
        } while(temp != null);
        
        OUT = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        
    }
    
    public String read(int lineNum) {
        currentPos = lineNum;
        return (String) inLines.get(lineNum);
    }
    
    public String readNext() {
        
        
        currentPos++;
        return read(currentPos);
        
        
    }
    public String reread() {
        return read(currentPos);
    }
    
    public int getNumOfLines() {
        return inLines.size();
    }
    
    public void addLine(String line) {
        outLines.add(line);
    }
    
    public String[] getTokens(int lineNumber) {
        StringTokenizer st = new StringTokenizer(this.reread());
        ArrayList<String> tokens = new ArrayList<String>();
        String temp;
        while(true) {
            try {
                temp = st.nextToken();
            } catch(NoSuchElementException e) {
                break;
            }
            
            
            tokens.add(temp);
            
            
        } 
        
        return tokens.toArray(new String[tokens.size()]);

    }
    
    public String[] getTokens() {
        return getTokens(currentPos);
    }
    
    
    public void submit() {
        for(String s: outLines) {
            OUT.println(s);
        }
        
        OUT.close();
        System.exit(0);
        
    }


}

public class gift1 {
    private static Giver[] friends;
    public static void main(String[] args) throws IOException{
        
        OlympiadIO io = new OlympiadIO("gift1");
        
        int numOfPeople = Integer.parseInt(io.read(0));
        
        friends = new Giver[numOfPeople];
        
        for(int i = 0; i < numOfPeople; i++) { //getting the names of the poeple
            
            
            friends[i] = new Giver(io.readNext());
        }
        
        for(int i = 0; i < numOfPeople; i++) {//gving timr
            int amountToGive;
            int amountOfFriends;
            Giver giver;//whose turn it is
            Giver[] friendsOfGiver;
            
            giver = find(io.readNext()); //finds the giver
            
            System.out.println(giver);
            io.readNext();
            String[] tokens = io.getTokens();//reads the amount ot give and amount of friends
            amountToGive = Integer.parseInt(tokens[0]);
            
            amountOfFriends = Integer.parseInt(tokens[1]);
            
            friendsOfGiver = new Giver[amountOfFriends];
            
            for(int j = 0; j < amountOfFriends; j++) {
                friendsOfGiver[j] = find(io.readNext());
            }
            
            giver.giveTo(friendsOfGiver,amountToGive);
        }
        
        //done with reading now
        
        for(Giver g: friends) {
            io.addLine(g.getName() + " " + g.getNetGain());
        }
        
        io.submit();
        
    }
    
    private static Giver find(String name) {
        for(Giver g: friends) {
            if(g.getName().equals(name))
                return g;
        }
        return null;
    }
}

class Giver {
    private int initialBalance; //can't be final because of dumb resaons
    private int balance = 0;
    private final String name;
    
    
    public Giver(String name) {
        this.name = name;
    }
    public void deduct(int money) {
        balance -= money;
    }
    
    public void gain(int money) {
        balance += money;
    }
    
    public String getName() {
        return name;
    }
    
    public int getNetGain() {
        return balance - initialBalance;
    }
    
    public void giveTo(Giver[] friends, int money) {// thsi should only be called once1
        if(friends.length == 0) 
            return;
        this.initialBalance = money;
        
        this.gain(money % friends.length);//leftover money
        
        int amountReceived = money / friends.length;
        
        for(Giver g: friends) {
            g.gain(amountReceived);
        }
    
        
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    

}