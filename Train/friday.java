/*NAME: johnwan3
 LANG: JAVA
 TASK: friday
 */

import java.io.*;
import java.util.*;



class OlympiadIO {
    private int currentPos = 0;
    private ArrayList<String> inLines = new ArrayList<String>();
    private final String name;
    
    private ArrayList<String> outLines = new ArrayList<String>();
    
    
    
    public OlympiadIO(String name) throws IOException{
        this.name = name;
        inputSetUp();
        
        
        
    }
    
    protected void inputSetUp() throws IOException{
        BufferedReader in;
        in = new BufferedReader(new FileReader(name + ".in"));
        
        String temp;
        do {
            temp = in.readLine();
            if(temp != null) {
                inLines.add(temp);
            }
            
        } while(temp != null);
    }
    protected ArrayList<String> getInLines() {return inLines; }
    protected ArrayList<String> getOutLines() {return outLines;}
    
    public String read(int lineNum) {
        currentPos = lineNum;
        return (String) inLines.get(lineNum);
    }
    
    public String readNext() {
        
        
        currentPos++;
        return read(currentPos);
        
        
    }
    public String reread() {
        return read(currentPos);
    }
    
    public int getNumOfLines() {
        return inLines.size();
    }
    
    public void addLine(String line) {
        outLines.add(line);
    }
    
    public String[] getTokens(int lineNumber) {
        StringTokenizer st = new StringTokenizer(this.reread());
        ArrayList<String> tokens = new ArrayList<String>();
        String temp;
        while(true) {
            try {
                temp = st.nextToken();
            } catch(NoSuchElementException e) {
                break;
            }
            
            
            tokens.add(temp);
            
            
        }
        
        return tokens.toArray(new String[tokens.size()]);
        
    }
    
    public String[] getTokens() {
        return getTokens(currentPos);
    }
    
    
    public void submit() throws IOException{
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(name + ".out")));
        for(String s: outLines) {
            out.println(s);
        }
        
        out.close();
        System.exit(0);
        
    }
    
    
}
public class friday {
    public static void main(String[] args) throws IOException{
        OlympiadIO io = newIO();
        
        
        int[] tally = new int[Day.DAYS_IN_A_WEEK];
        Year currentYear = null;
        for(int i = 0; i < Integer.parseInt(io.read(0)); i++) {
            currentYear = new Year(START_YEAR + i , (currentYear == null)? START_DAY : currentYear.getNextYearStart());
            tally = addArray(tally, currentYear.getBadDays());
        }
        
        String s = tally[Day.DAYS_IN_A_WEEK - 1] + ""; //saturday goes first because usaco is a weirdo and says saturday is the first
        
        for(int i = 1; i < Day.DAYS_IN_A_WEEK; i++) {
            s = s + " " + tally[i - 1];
        }
        
        io.addLine(s);
        
        io.submit();
        
        
        
    }
    private static OlympiadIO newIO() throws IOException{
        return new OlympiadIO("friday") /*{//comment out when submitting
            
            @Override
            public void submit() {
                for(String s : getOutLines()) {
                    System.out.println(s);
                }
            }
            
            @Override
            protected void inputSetUp() {
                this.getInLines()
                .addAll(Arrays.asList(
                                                       new String[] {
                                                           "20"
                                                       })
                        );
                                         
                                         
                                         
                                         
                        
            }
        }*/;
    }
    public static final int START_YEAR = 1900;
    public static final Day START_DAY = Day.MONDAY;
    
    
    private static int[] addArray(int[] arr1, int[] arr2) {
        
        if(arr1.length != Day.DAYS_IN_A_WEEK || arr2.length != Day.DAYS_IN_A_WEEK ){
            return null;
        }
        int[] newArray = new int[Day.DAYS_IN_A_WEEK];
        
        for(int i = 0; i < newArray.length; i++) {
            newArray[i] = arr1[i] + arr2[i];
            
        }
           return newArray;
    }
}

class Year {
    
    private final int year;
    private final boolean isLeap;
    private final int length;
    
    private final Day startDay;
    private final Day nextYearStart;
    public static final int DAYS_IN_NORMAL_YEAR = 365;
    public static final int MONTHS_IN_YEAR = 12;
    
   

    
    public Year(int year, Day startDay) {
        this.startDay = startDay;
        this.year = year;
        
        if(year % 400 == 0) {
            isLeap = true;
        } else if (year % 100 == 0) {
            isLeap = false;
        } else if (year % 4 == 0) {
            isLeap = true;
        } else {
            isLeap = false;
        }
        
        if(isLeap) {
            length = DAYS_IN_NORMAL_YEAR + 1;
        } else {
            length = DAYS_IN_NORMAL_YEAR;
        }
        
        nextYearStart = startDay.dayAfter(this.length);
            
        
        
    }
    
    public boolean isLeap() { return isLeap; }
    public Day getNextYearStart() { return nextYearStart;}
    
    public int[] getBadDays() {
        int[] badDays = new int[Day.DAYS_IN_A_WEEK]; //everything is moved down a spot because dumb computers count at 0
        Month currentMonth = null;//complier is being annohing
        
        for(int i = 1; i <= MONTHS_IN_YEAR; i++) {
            if(currentMonth == null) { //FIRST CASE
                currentMonth = new Month(startDay, i, this);
            } else {
                currentMonth = new Month(currentMonth.getNextMonthStart(), i, this);
            }
            badDays[currentMonth.getBadDay().getDayOfWeek() - 1]++;// -1 is because they java counts at 0
            
        }
        
        return badDays;
    }
    
    
    
    
    
    
    
}
 enum Day {
    SUNDAY (1),
    MONDAY (2),
    TUESDAY (3),
    WEDNESDAY (4),
    THURSDAY (5),
    FRIDAY (6),
    SATURDAY (7);

    private int dayOfWeek;
    public static final int DAYS_IN_A_WEEK = 7;


    Day(int day ) {
        dayOfWeek = day;
    }
    public int getDayOfWeek() {return dayOfWeek;}

    public Day dayAfter(int days) { //returns the day after a certain amount of days has passed
        int endDay = (days + this.getDayOfWeek() ) % DAYS_IN_A_WEEK ;

        switch (endDay) {
        case 1:
        return Day.SUNDAY;
        case 2:
        return Day.MONDAY;
        case 3:
        return Day.TUESDAY;
        case 4:
        return Day.WEDNESDAY;
        case 5:
        return Day.THURSDAY;
        case 6:
        return Day.FRIDAY;
        case 0: //because modulus goes to 0
        return Day.SATURDAY;
        default:
        return null;

    }



}



}

class Month {
    
    public static final int EVIL_NUM = 13;
    private final Day startDay;
    private final Day badDay;
    private final Day nextMonthStart;
    
    private final int numForm;
    private final int length;
    
    
    public Month(Day startDay, int month, Year year ) {
        this.startDay = startDay;
        numForm = month;
       
        
        switch (month){
            case 4: case 6: case 9: case 11:
                length = 30;
                break;
            case 2:
                if(year.isLeap())
                    length = 29;
                else
                    length = 28;
                break;
            default:
                length = 31;
                
        }
        badDay = startDay.dayAfter(EVIL_NUM - 1);//the thirteenth is actually twelve days after the first
        nextMonthStart = startDay.dayAfter(length);
        
    }
    
    public Day getNextMonthStart() { return nextMonthStart;}
    
    public Day getBadDay() {
        return badDay;
    }
    
    
    
    

}
